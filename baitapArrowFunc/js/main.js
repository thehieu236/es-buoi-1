const colorList = [
	'pallet',
	'viridian',
	'pewter',
	'cerulean',
	'vermillion',
	'lavender',
	'celadon',
	'saffron',
	'fuschia',
	'cinnabar',
];

renderButtonColor(colorList);

colorList.forEach((item) => {
	let getClassActive = document.querySelector(`.${item}`);

	getClassActive.addEventListener('click', (event) => {
		removeClass('active');
		getClassActive.classList.add('active');
		defaultHouseColor();

		document.getElementById('house').classList.add(item);
	});
});
