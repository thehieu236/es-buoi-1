function renderButtonColor(colorList) {
	let contentHTML = '';
	colorList.forEach((item) => {
		let contentBtnColor = `<button class="color-button ${item}"></button>`;

		if (item == 'pallet') {
			contentBtnColor = `<button class="color-button active ${item}"></button>`;
		}
		contentHTML += contentBtnColor;
	});

	document.getElementById('colorContainer').innerHTML = contentHTML;
}

// Hàm này dùng để check xem có class nào tồn tại không, nếu có thì xoá nó đi.
function removeClass(className) {
	let checkClass = document.querySelector(`.${className}`);

	checkClass.classList.remove(className);
}

function defaultHouseColor() {
	let checkClass = document.getElementById('house');
	let classList = checkClass.classList;

    // Tạo vòng lặp kiểm tra nếu có màu thì xoá nó đi để trở vế mặc định ban đầu.
	for (let i = 0; i < colorList.length; i++) {
		const item = colorList[i];
		for (let i = 0; i < classList.length; i++) {
			const e = classList[i];
			if (item == e) {
				classList.remove(item);
			}
		}
	}
}
