// Rest parameter,..
function tinhDiemTrungBinh(resultId, ...idList) {
	let val = 0;
	idList.forEach((item) => {
		let getValue = document.getElementById(item).value * 1;
		val += getValue;
	});
	let theAverage = val / idList.length;
	document.getElementById(resultId).innerHTML = theAverage.toFixed(2);
}
