let getClassHeader = document.querySelector('.heading');
let wordSeparator = getClassHeader.textContent;
let wordSeparatorStr = [...wordSeparator];

function renderWord() {
	let contentHTML = '';

	wordSeparatorStr.forEach((item) => {
		let contentHeading = `
    <span>${item}</span>
    `;
		contentHTML += contentHeading;
	});

	return (getClassHeader.innerHTML = contentHTML);
}

renderWord();
